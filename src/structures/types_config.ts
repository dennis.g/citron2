declare namespace Config {

    export interface DatabaseMongo {
        db_secure: boolean;
        db_serverip: string;
        db_serverport: number;
        db_database: string;
        db_username: string;
        db_password: string;
    }

    export interface DatabaseSql {
        db_serverip: string;
        db_serverport: number;
        db_database: string;
        db_username: string;
        db_password: string;
    }

    export interface Auth {
        user: string;
        pass: string;
    }

    export interface Tls {
        rejectUnauthorized: boolean;
        ciphers: string;
    }

    export interface Mail {
        host: string;
        port: number;
        secure: boolean;
        auth: Auth;
        tls: Tls;
    }

    export interface MailInfo {
        sender: string;
        sender_alias: string;
    }

    export interface Connection {
        port: number;
    }

    export interface Version {
        environment: string;
        release: string;
        server: string;
    }

    export interface Jsonwebtoken {
        privateKEY: string;
        publicKEY: string;
        issuer: string;
        subject: string;
        audience: string;
    }

    export interface RootObject {
        database_mongo?: DatabaseMongo;
        database_mysql: DatabaseSql;
        mail: Mail;
        mail_info: MailInfo;
        file_root: string;
        domain: string;
        connection: Connection;
        pw_salt: string;
        version: Version;
        jsonwebtoken: Jsonwebtoken;
    }

}

export {Config};