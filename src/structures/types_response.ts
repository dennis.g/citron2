declare namespace Response {

    type Action = 'success' | 'error';

    export interface Front_RootObject {
        action: Action;
        meta?: any;
        data?: any;
        error?: any;
    }
}


export {Response};