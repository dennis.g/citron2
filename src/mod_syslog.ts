import {sysConfig} from "./main"



class SysLog {

    private client:any;
    private syslog = require("syslog-client");
    private os = require("os");
    
    constructor(){
        var options = {
            syslogHostname: this.os.hostname(),
            transport: this.syslog.Transport.Udp,
            port: 514,
            appName: sysConfig.getName()
        };
        this.client = this.syslog.createClient("127.0.0.1", options);
    }

    public logInfo (text:string):void {
        var tmp = "[Info] " + text;
        this.client.log(tmp, {
          facility: this.syslog.Facility.Local2,
          severity: this.syslog.Severity.Informational
        }, function(error:any) {
          if (error) {
            console.error(error);
          }
        });
    }
      
      
    public logWarning (text:string):void {
        var tmp = "[Warn] " + text;
        this.client.log(tmp, {
          facility: this.syslog.Facility.Local2,
          severity: this.syslog.Severity.Warning
        }, function(error:any) {
          if (error) {
            console.error(error);
          }
        });
    }
      
      
    public logError (text:string):void {
        var tmp = "[Error] " + text;
        this.client.log(tmp, {
          facility: this.syslog.Facility.Local2,
          severity: this.syslog.Severity.Error
        }, function(error:any) {
          if (error) {
            console.error(error);
          }
        });
    }
      
    public logCritical (text:string):void {
        var tmp = "[Critical] " + text;
        this.client.log(tmp, {
          facility: this.syslog.Facility.Local2,
          severity: this.syslog.Severity.Critical
        }, function(error:any) {
          if (error) {
            console.error(error);
          }
        });
    }
      
    public logNotice (text:string):void {
        var tmp = "[Notice] " + text;
        this.client.log(tmp, {
          facility: this.syslog.Facility.Local2,
          severity: this.syslog.Severity.Notice
        }, function(error:any) {
          if (error) {
            console.error(error);
          }
        });
    } 

}

export {SysLog};