import fs from 'fs';
import os from 'os';
import util from 'util';

import {Config} from './structures/types_config';
import {sysLog} from './main'
import { pseudoRandomBytes } from 'crypto';


class ConfigModule {

    private config:Config.RootObject;
    private appName: string = "Citron Backend Engine";
    private appVersion: string = "v2.0.1";

    constructor(){

        this.config = <Config.RootObject> {};
        console.log(this.config);

    } //end constructor

    private isTest(arg: any): arg is Config.RootObject {
        return arg && arg.prop && typeof(arg.prop) == 'number';
    }

    public loadConfig():void {
        
        sysLog.logInfo("loading config.json");
        
        try{
            
            var raw:string = fs.readFileSync('../config.json',"utf8");
            
            this.config = JSON.parse(raw);
            
            sysLog.logInfo("Done loading config!");


        }catch(ex){
            console.log(ex);
            sysLog.logError("Error loading config.json");

        }

    }

    public getConfig():Config.RootObject {
        return this.config;
    }

    public getName():string {
        return this.appName;
    }

    public getVer():string {
        return this.appVersion;
    }

    public getInfo():any {
        return {
            'Product' : this.appName,
            'Version' : this.appVersion,
            'Vendor' : 'Dennis Gunia' ,
            'OS' : os.platform + " - " + os.release,
            'Server' : this.config.version
        };
    }

} //end class ConfigModule

export {ConfigModule, Config};