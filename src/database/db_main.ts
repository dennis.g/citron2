import * as main from '../main'
import mysql from 'mysql'
import {Config} from '../structures/types_config';
class SqlAdapter {
    private _sql_con!: mysql.Connection;


    constructor(){

        main.sysLog.logInfo("Connecting to database ...");

        let sqlConfData:Config.DatabaseSql = main.sysConfig.getConfig().database_mysql;

        //Create config object
        var sqlConData: mysql.ConnectionConfig = {
            host: sqlConfData.db_serverip,
            user: sqlConfData.db_username,
            password: sqlConfData.db_password,
            port: sqlConfData.db_serverport,
            database: sqlConfData.db_database
        }

        

        //validate config data
        if(!sqlConData.host || !sqlConData.user || !sqlConData.password || !sqlConData.database){

            main.sysLog.logError("Error connecting to database. Config error!");
            return;
        } 

        //open connection to database
        this._sql_con = mysql.createConnection(sqlConData);
        console.log(this._sql_con);
        this._sql_con.connect(function(err) {
            if (err) {
                
                main.sysLog.logError("Error connecting to database. Runtime error!");
                main.sysLog.logError(err.toString());
                return;
            }

            //only reached if no error has occured yet
            main.sysLog.logInfo("Connected!");
      
          });
        
    }

    public get state():string {
        return this._sql_con.state;
    }
}

export {SqlAdapter};