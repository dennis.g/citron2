import express from "express";
import {ConfigModule,Config} from './config'
import {SysLog} from "./mod_syslog"

import {Server} from "./server/server_main"
import {SqlAdapter} from "./database/db_main"

var sysConfig:ConfigModule = new ConfigModule();
var sysLog:SysLog = new SysLog();
sysConfig.loadConfig();
//console.log (sysConfig.getConfig());


var server = new Server();
server.setup();

var db = new SqlAdapter();

export {express,sysConfig,sysLog};




const app = express();