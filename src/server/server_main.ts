import * as main from '../main'
import * as Response from './server_response'
import { exit } from 'process';

var helmet = require('helmet');
var bodyParser = require("body-parser");

class Server {
    private app = main.express()
    constructor(){
        
        this.app.use(helmet());
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(bodyParser.json());

        this.app.disable('x-powered-by');
    }


    public setup():boolean{

        //get port from config
        var port:number = main.sysConfig.getConfig().connection.port;

        //test if port value is defined
        if (port === undefined ){
            port = 8080;
            main.sysLog.logError("Config:connection.port not defined. Using default value ( 8080 ).");
        }

        //start server
        this.app.listen(port, function() {
            main.sysLog.logInfo("Server listening @ 0.0.0.0:" + port);
          }).on('error', function(err) {
            main.sysLog.logCritical("Cant open server!");
            main.sysLog.logCritical(err.toString());
            return false;
          });



        this.app.get('/api/info', function(req, res) {
            Response.Front.write(res,main.sysConfig.getInfo(),undefined);
        });

        //default route
        this.app.get('*', function(req, res) {
            Response.Front.writeError(res,404,'Rescource nicht gefunden')
        });




        return true;

    }

}

export {Server};