import * as main from '../main'
import {Response} from '../structures/types_response'

module Front {
    export function writeHeader (res:main.express.Response,status:number):void {
        var header:any = {
            'Content-Type': 'application/json; charset=UTF-8',
            'allowedHeaders': ['sessionId', 'Content-Type'],
            'exposedHeaders': ['sessionId'],
            'origin': 'http://127.0.0.1:3000',
            'methods': 'GET,POST',
            'access-control-allow-credentials': 'true',
            'access-control-allow-origin': '*',
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        
            'preflightContinue': false
        };
        
        res.writeHead(status, header);
    }

    export function writeError(res:main.express.Response,status:number,message:string){
        writeHeader(res,status);
        
        var response:Response.Front_RootObject = {
            action: 'error',
            error: message
        };

        res.end(JSON.stringify(response));

    }

    export function write(res:main.express.Response,data:any,meta?:any){
        writeHeader(res,200);
        
        var response:Response.Front_RootObject = {
            action: 'success',
            meta: meta,
            data: data
        };

        res.end(JSON.stringify(response));

    }
}

module Dash {
    
}

export {Dash, Front}